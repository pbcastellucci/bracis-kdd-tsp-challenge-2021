(TeX-add-style-hook
 "sbc-template"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "12pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("algorithm2e" "linesnumbered" "ruled" "vlined")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art12"
    "graphicx"
    "url"
    "inputenc"
    "babel"
    "tikz"
    "subcaption"
    "algorithm2e"
    "indentfirst")
   (LaTeX-add-labels
    "sec:intro"
    "sec:iter-local-search"
    "alg:ils"
    "sec:local-search-phase"
    "fig:before-2-opt-move"
    "fig:after-2-opt-move"
    "fig:2-opt"
    "sec:perturbation-phase"
    "sec:comp-comm")
   (LaTeX-add-bibliographies))
 :latex)

