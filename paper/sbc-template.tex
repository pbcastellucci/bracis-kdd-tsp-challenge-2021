\documentclass[12pt]{article}

\usepackage{sbc-template}
\usepackage{graphicx,url}
\usepackage[utf8]{inputenc}
\usepackage[]{babel}
%\usepackage[latin1]{inputenc}  
\usepackage{tikz}
\usepackage{subcaption}
\usepackage[linesnumbered,ruled,vlined]{algorithm2e}
\usepackage{indentfirst}

\sloppy

\title{An iterated local search for the travelling salesman problem}

\author{Pedro B. Castellucci\inst{1} }

\address{Departamento de Informática e Estatística\\ Universidade Federal de Santa Catarina (UFSC)\\
  Florianópolis -- SC -- Brazil
  \email{\{pedro.castellucci\}@ufsc.br}
}

\begin{document} 

\maketitle

\begin{abstract}
% Max 10 lines
  
The Traveling Salesman Problem is a classical problem in Computer Science, with many extensions and
variations being studied along decades of research, particularly for applications related to
logistics. Here, we present a Iterated Local Search (ILS) algorithm to find solutions for instances of the problem
as proposed by The Brazilian competition on Knowledge Discovery in Databases (KDD-BR). The ILS
algorithm is a simple algorithm that does not depend on any third-party libraries. Also, it was among
most effective methods in the competition.
\end{abstract}


\section{Introduction}
\label{sec:intro} 


The Traveling Salesman Problem (TSP) is one of the most studied problems in Computer Science. The
problem can be defined based on a graph $G=(V,E)$ in which $V$ is the set of nodes (e.g. cities) and $E$ the set
of edges connecting these nodes. Associated with each edge there is a cost $c_{ij} \geq 0$. The goal
is to find a tour $r = (i_1, i_2, \ldots, i_{|V|}, i_1)$ which visits each node exactly once and has a
minimum cost.

Despite the NP-hardness of the problem, state-of-the-art exact methods can currently solve, with
proved optimality, instances with several thousands of nodes. These methods are based on the
branch-and-cut framework from integer programming \cite{applegate2011traveling}. Regarding heuristic
methods, LKH-based heuristics achieve state-of-the-art results \cite{tinos2018efficient}. One
important component of LKH heuristic is the k-Opt
movements, which we used for this competition. These moves are well known in the TSP literature
\cite{applegate2011traveling} and for other routing problems. 

More recently, there has been an emergent trend of applying machine learning techniques to combinatorial
NP-hard problems, particularly for the TSP (\cite{ishaya2019comparative}, \cite{joshi2019efficient}
and \cite{vitali2021machine}). There have been some interesting results but these techniques are yet
to be competitive with state-of-the-art methods.

The goal of the competition was presented as follows: \textit{``The objective of this challenge is
  to develop a method to predict the edges belonging to solutions of traveling salesman problems.''}

Aiming at this goal, we implemented a solution based on a metaheuristic known as \textit{Iterated Local
  Search} (ILS). Then, we
use the solution provided by the ILS as a predictor of the edges in an optimal solution. As an
abstract method, this is similar to techniques such as Neural Networks. It tries to find a
solution that optimizes a function (the cost of a route, in our ILS idea) and uses this solution to
define a prediction.  

The ILS algorithm is described
in Section \ref{sec:iter-local-search}, with its components in
subsections~\ref{sec:local-search-phase} and \ref{sec:perturbation-phase}. Then, we offer
some comments regarding the computational experiments and implementation in Section \ref{sec:comp-comm}.


\section{Iterated Local Search}
\label{sec:iter-local-search}

The Iterated Local Search is a simple metaheuristic but with competitive results for complex
combinatorial problems \cite{lourencco2019iterated}. It is also flexible, being applied not only
to routing problems (e.g. \cite{stutzle2002analysing}, \cite{vansteenwegen2009iterated} and
\cite{penna2013iterated}) but to others such as
timetabling problems (e.g. \cite{song2018iterated}) and packing problems (e.g. \cite{imamichi2009iterated}).

To apply the ILS metaheuristic, a problem specific algorithm should be known, then it is used as a local
search procedure. This local search is combined with a perturbation phase to restart the search with
the intent of avoiding local optimal solutions. Algorithm \ref{alg:ils} presents a pseudo-code of our
ILS based solution. 

\begin{algorithm}[H]
  \DontPrintSemicolon
  \KwData{An initial solution $s_0$ and a maximum number of iterations $maxIter$.}
  \KwResult{A solution $s^*$.}
  $s^*$: LocalSearch($s_0$)  \tcc{See Subsection \ref{sec:local-search-phase}.}
  $iter = 0$\;
  \While{$iter < maxIter$}{
    $s'$ = Perturbation($s^*$, $iter$) \tcc{See Subsection \ref{sec:perturbation-phase}} 
    $s^{*'}$ = LocalSearch($s'$)\; 
    \If{$f(s^{*'}) < f(s^*)$}{
      $s^* = s^{*'}$\;
      $iter = 0$\;
     }
    $iter = iter + 1$\;
  }
  \caption{Iterated local search}
  \label{alg:ils}
\end{algorithm}

The ILS algorithm has as an input an initial solution. For instances with $n$ nodes, we used $(1, 2,
\ldots, n)$ which is sorted as provided by the challenge organization. The first step (line 1 in
Algorithm \ref{alg:ils}) is a local search based on k-opt movements, described in Subsection
\ref{sec:local-search-phase}. After the local search finds a local optimum, the main loop (lines 3--9)
alternates between a perturbation phase (line 4) and a local search (line 5). The perturbation
procedure tries to escape the local optimal by disturbing the current solution (details in
Subsection \ref{sec:perturbation-phase}). Then, lines 6--8, account for saving the best solution
found so far. Every time there is a solution improvement, the iteration count is reset (line 8).

\subsection{Local search phase}
\label{sec:local-search-phase}

For the local search phase, we used $k$-opt movements, with $k = 2$ and $k = 3$. A $k$-opt
movement consists of replacing $k$ edges of a solution for different ones. For the 2-opt move, for
example, there is only one way to recreate a tour (Figure
\ref{fig:2-opt}). For the 3-opt movements there are seven different ways (three of them equivalent to
2-opt moves), we try each combination to find the one with the least cost.  

\begin{figure}[htpb]
  \centering
  \begin{subfigure}{0.45\textwidth}
    \centering
    \begin{tikzpicture}[
      every node/.style={}
      ]
      \node[circle, draw](a) at (0, 0) {};
      \node at (1, 1.4) {$i$};
      \node[circle, draw] (b) at (1, 0.9) {};
      \node at (3, 1.4) {$j+1$};
      \node[circle, draw](c) at (3, 0.9) {};
      \node[circle, draw](d) at (4, 0) {};
      \node[] at (1, -1.4) {$j$};
      \node[circle, draw](e) at (1, -0.9) {};
      \node at (3, -1.4) {$i+1$};
      \node[circle, draw](f) at (3, -0.9) {};
      
      \draw[->, thick] (a) to (b);
      \draw[->, thick] (b) to (f);
      \draw[->, thick] (f) to (d);
      \draw[->, thick] (d) to (c);
      \draw[->, thick] (c) to (e);
      \draw[->, thick] (e) to (a);    
    \end{tikzpicture}
    \caption{Before a 2-opt move.}
    \label{fig:before-2-opt-move}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.45\textwidth}
    \centering
    \begin{tikzpicture}[
      every node/.style={}
      ]
      \node[circle, draw](a) at (0, 0) {};
      \node at (1, 1.4) {$i$};
      \node[circle, draw] (b) at (1, 0.9) {};
      \node at (3, 1.4) {$j+1$};
      \node[circle, draw](c) at (3, 0.9) {};
      \node[circle, draw](d) at (4, 0) {};
      \node[] at (1, -1.4) {$j$};
      \node[circle, draw](e) at (1, -0.9) {};
      \node at (3, -1.4) {$i+1$};
      \node[circle, draw](f) at (3, -0.9) {};
      
      \draw[->, thick] (a) to (b);
      \draw[->, thick] (b) to (c);
      \draw[->, thick] (c) to (d);
      \draw[->, thick] (d) to (f);
      \draw[->, thick] (f) to (e);
      \draw[->, thick] (e) to (a);
    \end{tikzpicture}
    \caption{After a 2-opt move.}
    \label{fig:after-2-opt-move}
  \end{subfigure}
  \caption{Example of a 2-opt move, removing edges $(i, i+1)$ and $(j, j+1)$.}
  \label{fig:2-opt}
\end{figure}


The local search phase is a maximum descent algorithm. It searches for the pair of edges the minimize
the total cost of the tour when applying a 2-opt movement. Then, it searches for the three edges
that minimize the total cost of the tour when applying a 3-opt movement. After the maximum descent
algorithm, the solution is a local optimum in the neighborhood defined by the 3-opt
movements. Finally, we perturb the solution (Subsection \ref{sec:perturbation-phase}) to try to escape this local optimum.

Note that, even though 2-opt movements are included in 3-opt movements, we first apply 2-opt followed
by the remaining 3-opt moves. From empirical observations, this resulted in a faster algorithm, probably because of
the reduced number of 3-opt movements needed to reach a local optimal. 


\subsection{Perturbation phase}
\label{sec:perturbation-phase}

To define the perturbation strategies, consider a tour $r$. Let $r[i]$ be the i-th city in the tour
and $r[i:j]$ being the path from $i$ to $j$ (both included). We defined three strategies:
\textit{(i)} Randomly sample $i$ from $[1: n-gap]$ and $j$ from $[i+gap, n]$ and shuffle the cities
in path $r[i:j]$. \textit{(ii)} Randomly sample $i$ from $[1: n-gap]$ and $j$ from $[i+gap, n]$, shuffle the cities
in path $r[i:j]$ and randomly reposition path $r[i:j]$ along the route. The $gap$ parameter is used
as 30 in our solution. Finally, \textit{(iii)} shuffle route $r$ completely.


Each perturbation strategy is applied at different moments during the search. Strategy \textit{(i)}
is applied when $0 \leq iter < 400$, strategy \textit{(ii)} when $400 \leq iter < 1500$ and strategy
\textit{(iii)} when $iter \geq 1500$ (see Algorithm \ref{alg:ils}). 

The algorithm terminates when the maximum number of iterations without any improvement is reached
($maxIter = 2500$). The edges in the best route found $s^*$ are then used as a predictor for the edges in
an optimal solution, i.e., if $(i, j)$ is part of route $s^*$ then we assume is part of the optimal
solution. 

\section{Computational remarks}
\label{sec:comp-comm}

According to challenge rules, the metric for evaluating the results is the F1 metric of the
predicted adjacency matrix compared to the optimal solution of each of the 2000 instances. This metric was computed on a fraction
of the test set and the result was 0.07906 for the Iterated Local Search based strategy presented in
Section \ref{sec:iter-local-search}.

The source code is available at
\url{https://gitlab.com/pbcastellucci/bracis-kdd-tsp-challenge-2021}. It was implemented in about
300 lines of Julia code and does not depend on any third-party library. Regarding the computational
enviroment, we used Linux Mint 20 OS and ran the experiments in an Intel(R) Core$^{TM}$ i5-8265U 4x1.60GHz, 8GB of RAM.

Regarding efficiency, the prediction is generated in seconds for each instance. The size of the
instances varies from 50 to 200 city nodes. Due to the nature of the competition, we did not focus on
precisely measuring the computational time nor the contribution of each component of the solution,
which could be interesting for better characterizing the algorithm. Also, there are potential
improvements to be made in tuning the parameters of the algorithm. This tuning could use the
training set provided. 


\section*{Acknowledgments}

We thank the organizers of the 5th edition of the Brazilian competition on Knowledge
Discovery in Databases, the reviewers and the sponsors of the event.

\bibliographystyle{sbc}
\bibliography{sbc-template}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
