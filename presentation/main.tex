\documentclass[]{beamer}
%
% Choose how your presentation looks.
%
% For more themes, color themes and font themes, see:
% http://deic.uab.es/~iblanes/beamer_gallery/index_by_theme.html
%
\mode<presentation>
{
  \usetheme{default}      % or try Darmstadt, Madrid, Warsaw, ...
  \usecolortheme{default} % or try albatross, beaver, crane, ...
  \usefonttheme{default}  % or try serif, structurebold, ...
  \setbeamertemplate{navigation symbols}{}
  \setbeamertemplate{caption}[numbered]
} 



\setbeamercolor{title}{fg=black}
\setbeamercolor{frametitle}{fg=black}

%\usepackage[portuguese]{babel}
\usepackage[]{babel} 

\usepackage[utf8x]{inputenc}
\usepackage{tikz}
\usepackage{xcolor}
\usepackage{changepage}
\usepackage[round]{natbib}
\usepackage{subcaption}
\usepackage[linesnumbered,ruled,vlined]{algorithm2e}
\usepackage{tikz}
\usepackage{tikz-qtree,tikz-qtree-compat}
\usepackage{pgfplots}

\pgfplotsset{width=7cm,compat=1.8}
\usepgfplotslibrary{fillbetween}

\pgfmathdeclarefunction{gauss}{2}{%
  \pgfmathparse{1/(#2*sqrt(2*pi))*exp(-((x-#1)^2)/(2*#2^2))}%
}

\newcommand{\myDefinitionTableColumns}{p{0.09\textwidth}p{0.85\textwidth}}

\linespread{1.3}

\newcommand{\specialcell}[2][c]{%
  \begin{tabular}[#1]{@{}c@{}}#2\end{tabular}}

\title[Your Short Title]{
\normalsize Iterated Local Search for the Traveling Salesman Problem\\
\vspace{0.6cm}
\footnotesize Pedro Belin Castellucci\\
\vspace{0.6cm}
\includegraphics[scale=0.07]{images/Ensino-colorida-horizontal.png}
}

\author{}

\institute{}
    
\date{\footnotesize 5th Brazilian Competition on Knowledge Discovery in Databases}

\begin{document}
\begin{frame}
\maketitle
\end{frame}

\begin{frame}{The challenge}
    The objective of this challenge is to develop a method to predict the edges belonging to optimal
    solutions of traveling salesman problems.
\end{frame}

\begin{frame}{The general approach}
  \vfill
  \begin{enumerate}
  \item Search for local optimum of an instance.
  \item Assume a local optimum solution is ``close'' to a global optimum.
  \end{enumerate}
  \vfill
  To find a local optimum solution, we use the general 2-opt and 3-opt movements.
  \vfill
\end{frame}

\begin{frame}
  \frametitle{2-opt}
  \begin{figure}[htpb]
    \centering
    \begin{subfigure}{0.45\textwidth}
      \centering
      \begin{tikzpicture}[
        every node/.style={}
        ]
        \node[circle, draw](a) at (0, 0) {};
        \node at (1, 1.4) {$i$};
        \node[circle, draw] (b) at (1, 0.9) {};
        \node at (3, 1.4) {$j+1$};
        \node[circle, draw](c) at (3, 0.9) {};
        \node[circle, draw](d) at (4, 0) {};
        \node[] at (1, -1.4) {$j$};
        \node[circle, draw](e) at (1, -0.9) {};
        \node at (3, -1.4) {$i+1$};
        \node[circle, draw](f) at (3, -0.9) {};
        
        \draw[->, thick] (a) to (b);
        \draw[->, thick] (b) to (f);
        \draw[->, thick] (f) to (d);
        \draw[->, thick] (d) to (c);
        \draw[->, thick] (c) to (e);
        \draw[->, thick] (e) to (a);    
      \end{tikzpicture}
      \caption{Before a 2-opt move.}
      \label{fig:before-2-opt-move}
    \end{subfigure}
    \hfill
    \begin{subfigure}{0.45\textwidth}
      \centering
      \begin{tikzpicture}[
        every node/.style={}
        ]
        \node[circle, draw](a) at (0, 0) {};
        \node at (1, 1.4) {$i$};
        \node[circle, draw] (b) at (1, 0.9) {};
        \node at (3, 1.4) {$j+1$};
        \node[circle, draw](c) at (3, 0.9) {};
        \node[circle, draw](d) at (4, 0) {};
        \node[] at (1, -1.4) {$j$};
        \node[circle, draw](e) at (1, -0.9) {};
        \node at (3, -1.4) {$i+1$};
        \node[circle, draw](f) at (3, -0.9) {};
        
        \draw[->, thick] (a) to (b);
        \draw[->, thick] (b) to (c);
        \draw[->, thick] (c) to (d);
        \draw[->, thick] (d) to (f);
        \draw[->, thick] (f) to (e);
        \draw[->, thick] (e) to (a);
      \end{tikzpicture}
      \caption{After a 2-opt move.}
      \label{fig:after-2-opt-move}
    \end{subfigure}
    \caption{Example of a 2-opt move, removing edges $(i, i+1)$ and $(j, j+1)$.}
    \label{fig:2-opt}
  \end{figure}
\end{frame}

\begin{frame}
  \frametitle{3-opt}
  \begin{figure}[htpb]
    \centering
    \includegraphics[scale=0.7]{images/3opt.png}
     \caption{3-opt movements \citep{ismkhan2010using}.}
    \label{fig:3opt}
  \end{figure}
\end{frame}


\begin{frame}
  \frametitle{Local search}
  \centering
  \begin{figure}[htpb]
    \centering
    \includegraphics[scale=1.4]{images/ils_first.png}
    \caption{Local search (adapted from \cite{lourencco2019iterated}).}
    \label{fig:ils-idea}
  \end{figure}
  \vfill
  For computational efficiency, we used 2-opt followed by 3-opt.
  \vfill
\end{frame}

\begin{frame}
  \frametitle{The ILS metaheuristic}
  \centering
  \begin{figure}[htpb]
    \centering
    \only<1>{\includegraphics[scale=1.4]{images/ils_first.png}}
    \only<2>{\includegraphics[scale=1.4]{images/ils_complete.png}}
    \caption{The Iterated Local Search idea (adapted from \cite{lourencco2019iterated}.}
    \label{fig:ils-idea}
  \end{figure}
\end{frame}

\begin{frame}
  \frametitle{Perturbation procedures}
  \vfill

  Given a route $r$, with $r[i]$ being the $i$-th city in the tour and $r[i:j]$ the path between
  $r[i]$ and $r[j]$ (both included).
  
  \vfill  
  \begin{enumerate}
  \item[] \textbf{Strategy 1}. Sample $i \in [1, n-gap]$ and $j \in [i+gap, n]$ and shuffle the path
    $r[i:j]$. 
  \item[] \textbf{Strategy 2}. Sample $i \in [1, n-gap]$ and $j \in [i+gap, n]$ and shuffle the path
    $r[i:j]$. Then randomly reposition path $r[i:j]$ along route $r$.
  \item[] \textbf{Strategy 3}. Shuffle $r$ completely. 
  \end{enumerate}
  \vfill
  For simplicity, we chose $gap = 30$. 
  \vfill
\end{frame}

\begin{frame}
  \frametitle{The ILS algorithm}
  \footnotesize
  \begin{algorithm}[H]
    \DontPrintSemicolon
    \KwData{An initial solution $s_0$ and a maximum number of iterations $maxIter$.}
    \KwResult{A solution $s^*$.}
    $s^*$: LocalSearch($s_0$) \tcc{Maximum descent.}
    $iter = 0$\;
    \While{$iter < maxIter$}{
      $s'$ = Perturbation($s^*$, $iter$) \;
      $s^{*'}$ = LocalSearch($s'$)\; 
      \If{$f(s^{*'}) < f(s^*)$}{
        $s^* = s^{*'}$\;
        $iter = 0$\;
      }
      $iter = iter + 1$\;
    }
    \caption{Iterated local search}
    \label{alg:ils}
  \end{algorithm}
\end{frame}

\begin{frame}
  \frametitle{Parameters}
  \vfill
  Parameter $gap$ is set to $30$.
  \vfill
  The number of iterations without improvements $maxIter$ is set to $2500$.
  \vfill
  Choice of the perturbation procedure:
  \begin{enumerate}
  \item [] if $iter < 400$ use strategy 1.
  \item [] else if $iter < 1500$ use strategy 2.
  \item [] else $iter < 2500$ use strategy 3.
  \end{enumerate}
  \vfill
  These parameters could be tuned using the training set.
  \vfill
\end{frame}

\begin{frame}
  \frametitle{Remarks}
  \vfill
  Computational time was not measured precisely.
  \vfill
  Contribution of each component of the algorithm could be analyzed.
  \vfill
  Solution developed in Julia.
  \vfill
  Does not depend on any third-party library.
  \vfill
  Code is fully available online: \url{gitlab.com/pbcastellucci/bracis-kdd-tsp-challenge-2021}
  \vfill
\end{frame}

\begin{frame}
  \maketitle
\end{frame}

\bibliographystyle{plainnat}

\begin{frame}[allowframebreaks]{References}
  \footnotesize
\nocite{*}
\bibliography{sample.bib}    
\end{frame}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
